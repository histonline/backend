<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use Firebase\JWT\JWT;

require './vendor/autoload.php';

$app = new \Slim\App;

// print_r($_SERVER["SERVER_NAME"]);

if($_SERVER["SERVER_NAME"] == "localhost"){
    // Development
    $_ENV["host"] = "localhost";
    $_ENV["db"] = "histonline";
    $_ENV["user"] = "root";
    $_ENV["pass"] = "Generaltech";
}else{
    // Production
    $_ENV["host"] = "http://mysql_prod08.intranet.ufba.br";
    $_ENV["db"] = "histonlinedb";
    $_ENV["user"] = "histonlinemng";
    $_ENV["pass"] = "histonlinemn6!12!17";
}

$myPDO = new PDO("mysql:host={$_ENV["host"]};dbname={$_ENV["db"]}", "{$_ENV["user"]}", "{$_ENV["pass"]}");
$myPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$myPDO->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

/**
 * Auth básica do JWT
 * Whitelist - Bloqueia tudo, e só libera os
 * itens dentro do "passthrough"
 */
$app->add(new Tuupola\Middleware\JwtAuthentication([
    "regexp" => "/(.*)/", //Regex para encontrar o Token nos Headers - Livre
    "header" => "X-Token", //O Header que vai conter o token
    "path" => "/api", //Vamos cobrir toda a API a partir do /
    "realm" => "Protected", 
    "secret" => "teste" //Nosso secretkey criado 
]));

$app->add(function ($request, $response, $next) {
    $response = $next($request, $response);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
            ->withHeader('Content-Type', 'application/json');
});

/**
 * Rota que fala sobre a api
 */
$app->get('/', function ($request, $response) {
    $return = $response->withJson(array(
        "titulo" => "Rest API HistOnline",
        "descricao" => "Esse é o backend do serviço do HistOnline, o atlas de histologia online interativo, direcionados à estudantes de medicina da Universidade Federal da Bahia, servido no estilo API Rest"
    ), 200);
    return $return;
});

/**
 * HTTP Auth - Autenticação minimalista para retornar um JWT
 */
$app->post('/auth', function ($request, $response, $args) {
    $getBody = $request->getParsedBody();
    $username = $getBody["username"];
    $password = md5($getBody["password"]);
    // Verifica se usuário e senha coincidem
    $key = "teste";

    try{
        global $myPDO;
        $sql_query = "SELECT usuario, senha FROM usuarios WHERE usuario = '$username' AND senha = '$password' ";

        $statement = $myPDO->query($sql_query);

        if($statement->rowCount()){

            // $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            $token = array(
                "user" => $username
            );
        
            $jwt = JWT::encode($token, $key);
            $response = $response->withJson(array(
                "access_token" => $jwt ),
            200);

        }else{
            $response = $response->withJson(array(
                "tipo" => "Erro de autenticação",
                "mensagem" => "Usuário ou senha não existente" ), 
            403);            
        }

        return $response;

    }catch(PDOException $e){
        return $response->withJson(array(
            "erro" => $e->getMessage()
        ));
    }
});

/**
 * Teste de página protegida
 */
$app->get('/api', function ($request, $response) use ($app) {
    return $response->withJson(["msg" => "pagina protegida"], 200);  
});


$app->run();